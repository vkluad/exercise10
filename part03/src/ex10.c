#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/ctype.h>
#include <linux/slab.h>
#include <linux/kern_levels.h>

#define LEN_MSG 2
static char buf_msg[LEN_MSG + 1] = "0\n";

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vlad Kudenchuk <vkluad@gmail.com>");
MODULE_DESCRIPTION("Exercise 10");
MODULE_VERSION("0.1");

#define MODULE_TAG "ex10"
#define PROC_DIRECTORY "ex10"
#define PROC_FILENAME "buffer"
#define BUFFER_SIZE 64

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;
static long l_mode;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t ex10_read(struct file *file_p, char __user *buffer,
			 size_t length, loff_t *offset);
static ssize_t ex10_write(struct file *file_p, const char __user *buffer,
			  size_t length, loff_t *offset);
static void reverse(char s[]);
static void uppercase(char s[]);

static struct proc_ops proc_fops = { .proc_read = ex10_read,
				     .proc_write = ex10_write };

static int create_buffer(void)
{
	proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
	if (NULL == proc_buffer)
		return -ENOMEM;
	proc_msg_length = 0;

	return 0;
}

static void cleanup_buffer(void)
{
	if (proc_buffer) {
		kfree(proc_buffer);
		proc_buffer = NULL;
	}
	proc_msg_length = 0;
}

static int create_proc_example(void)
{
	proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
	if (NULL == proc_dir)
		return -EFAULT;

	proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO,
				proc_dir, &proc_fops);
	if (NULL == proc_file)
		return -EFAULT;

	return 0;
}

static void cleanup_proc_example(void)
{
	if (proc_file) {
		remove_proc_entry(PROC_FILENAME, proc_dir);
		proc_file = NULL;
	}
	if (proc_dir) {
		remove_proc_entry(PROC_DIRECTORY, NULL);
		proc_dir = NULL;
	}
}

void reverse(char s[])
{
	int i, j;
	char c;
	char *string = kstrdup(s, GFP_USER), *toks;
	strncpy(s, "", strlen(s));
	while ((toks = strsep(&string, " \n")) != NULL) {
		for (i = 0, j = strlen(toks) - 1; i < j; i++, j--) {
			c = toks[i];
			toks[i] = toks[j];
			toks[j] = c;
		}
		strcat(s, toks);
		strcat(s, " ");
	}
	s[strlen(s) - 2] = '\n';

	kfree(string);
}

void uppercase(char s[])
{
	char *temp = s;
	while(*temp){
		*temp = toupper((unsigned char) *temp);
		temp++;
	}
}

static ssize_t ex10_read(struct file *file_p, char __user *buffer,
			 size_t length, loff_t *offset)
{
	size_t left;

	if (length > (proc_msg_length - proc_msg_read_pos))
		length = (proc_msg_length - proc_msg_read_pos);

	if (l_mode == 1)
		reverse(&proc_buffer[proc_msg_read_pos]);

	if (l_mode == 2)
		uppercase(&proc_buffer[proc_msg_read_pos]);

	left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);
	printk(KERN_NOTICE MODULE_TAG ": Buffer: %s", buffer);
	proc_msg_read_pos += length - left;

	if (left)
		printk(KERN_ERR MODULE_TAG
		       ": failed to read %uu from %u chars\n",
		       left, length);
	else
		printk(KERN_NOTICE MODULE_TAG ": read %u chars\n", length);

	return length - left;
}

static ssize_t ex10_write(struct file *file_p, const char __user *buffer,
			  size_t length, loff_t *offset)
{
	size_t msg_length;
	size_t left;

	if (length > BUFFER_SIZE) {
		printk(KERN_WARNING MODULE_TAG
		       ": reduse message length from %u to %u chars\n",
		       length, BUFFER_SIZE);
		msg_length = BUFFER_SIZE;
	} else
		msg_length = length;

	left = copy_from_user(proc_buffer, buffer, msg_length);

	proc_msg_length = msg_length - left;
	proc_msg_read_pos = 0;

	if (left)
		printk(KERN_ERR MODULE_TAG
		       ": failed to write %u from %u chars\n",
		       left, msg_length);
	else
		printk(KERN_NOTICE MODULE_TAG ": written %u chars\n",
		       msg_length);

	return length;
}

static ssize_t ex10_show(struct class *class, struct class_attribute *attr,
			 char *buf)
{
	strcpy(buf, buf_msg);
	printk(": read %ld\n", (long)strlen(buf));
	return strlen(buf);
}

static ssize_t ex10_store(struct class *class, struct class_attribute *attr,
			  const char *buf, size_t count)
{
	printk(KERN_NOTICE MODULE_TAG ": write %ld\n", (long)count);
	strncpy(buf_msg, buf, count);
	if (kstrtol(buf, 10, &l_mode) == 0) {
		if (l_mode < 0)
			l_mode = 0;
		if (l_mode >= 2)
			l_mode = 2;

		printk(KERN_NOTICE MODULE_TAG ": Mode: %ld\n", l_mode);
	}
	buf_msg[count] = '\0';
	return count;
}

CLASS_ATTR_RW(ex10);

static struct class *x_class;

int __init ex10_init(void)
{
	int err, res;

	err = create_buffer();
	if (err)
		goto error;

	err = create_proc_example();
	if (err)
		goto error;

	printk(KERN_NOTICE MODULE_TAG ": loaded\n");
	x_class = class_create(THIS_MODULE, "ex10");
	if (IS_ERR(x_class))
		printk(KERN_ERR MODULE_TAG ": bad class create\n");
	res = class_create_file(x_class, &class_attr_ex10);
	printk(KERN_NOTICE MODULE_TAG ": module initialized\n");
	return res;

error:
	printk(KERN_ERR MODULE_TAG ": failed to load\n");
	cleanup_proc_example();
	cleanup_buffer();
	return err;
}

void __exit ex10_exit(void)
{
	class_remove_file(x_class, &class_attr_ex10);
	class_destroy(x_class);
	cleanup_proc_example();
	cleanup_buffer();
	printk(KERN_NOTICE MODULE_TAG ": exited\n");
}

module_init(ex10_init);
module_exit(ex10_exit);
